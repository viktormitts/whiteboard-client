export default interface User {
  id: string
  username: string
}

export interface InvitableUser {
  user: User
  sent: boolean
}

export interface Invitation {
  sessionID: string
  host: User
}
