import type { Drawing, Vec2 } from '../types/Item'

class Pen {
  ctx: CanvasRenderingContext2D
  active: boolean

  constructor(ctx: CanvasRenderingContext2D) {
    this.ctx = ctx
    this.active = false
  }

  drawLine(start: Vec2, end: Vec2): [Vec2, Vec2] {
    // Path style
    this.ctx.strokeStyle = '#000'
    this.ctx.lineWidth = 5
    this.ctx.lineCap = 'round'
    this.ctx.globalCompositeOperation = 'source-over'

    this.stroke(start, end)

    return this.createEdge(start, end)
  }

  eraseLine(start: Vec2, end: Vec2): [Vec2, Vec2] {
    // Path style
    this.ctx.strokeStyle = '#fff'
    this.ctx.lineWidth = 20
    this.ctx.lineCap = 'square'
    this.ctx.globalCompositeOperation = 'destination-out'

    this.stroke(start, end)

    return this.createEdge(start, end)
  }

  stroke(start: Vec2, end: Vec2): void {
    this.ctx.beginPath()
    this.ctx.moveTo(start.x, start.y)
    this.ctx.lineTo(end.x, end.y)
    this.ctx.stroke()
    this.ctx.closePath()
  }

  createEdge(start: Vec2, end: Vec2): [Vec2, Vec2] {
    const edge: [Vec2, Vec2] = [{...start}, {...end}]
    return edge
  }

  drawEdge(edge: [Vec2, Vec2]): void {
    this.drawLine(edge[0], edge[1])
  }

  eraseEdge(edge: [Vec2, Vec2]): void {
    this.eraseLine(edge[0], edge[1])
  }

  drawPath(path: Drawing): void {
    // Path style
    this.ctx.strokeStyle = path.strokeStyle.toString()
    this.ctx.lineWidth = path.lineWidth
    this.ctx.lineCap = path.lineCap
    this.ctx.globalCompositeOperation = <GlobalCompositeOperation>(
      path.globalCompositeOperation
    )

    if (path.strokeStyle === '#000000') {
      path.edges.forEach(edge => {
        this.drawEdge(edge)
      })
    } else {
      path.edges.forEach(edge => {
        this.eraseEdge(edge)
      })
    }
  }
}

export default Pen
