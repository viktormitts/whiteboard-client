import type { Vec2 } from '../types/Item'

const NOTE_OFFSET = 50

class NoteTool {
  pos: Vec2

  constructor() {
    this.pos = { x: 0, y: 0 }
  }

  move(x: number, y: number): void {
    const note: HTMLElement = document.getElementById('note')
    note.style.left = x - NOTE_OFFSET + 'px'
    note.style.top = y - NOTE_OFFSET + 'px'
    this.pos.x = x - NOTE_OFFSET
    this.pos.y = y - NOTE_OFFSET
  }

  onMouseMove(x: number, y: number): void {
    this.move(x, y)
  }
}

export default NoteTool
