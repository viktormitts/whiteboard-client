import domToImage from 'dom-to-image'
import { saveAs } from 'file-saver'

function hideElements(): void {
  const collection = document.getElementsByClassName('hide')
  for (let i = 0; i < collection.length; i++) {
    const el: HTMLElement = collection[i] as HTMLElement
    el.style.display = 'none'
  }
}

function showElements(): void {
  const collection = document.getElementsByClassName('hide')
  for (let i = 0; i < collection.length; i++) {
    const el: HTMLElement = collection[i] as HTMLElement
    el.style.display = 'initial'
  }
}

export function exportAs(suffix: string): void {
  hideElements()
  const board: HTMLElement = document.getElementById('board')
  if (suffix === 'png') {
    exportAsPNG(board)
  } else if (suffix === 'jpeg') {
    exportAsJPEG(board)
  }
}

export function exportWhiteboard(filename: string, suffix: string, callback: () => any): void {
  hideElements()
  const board: HTMLElement = document.getElementById('board')
  if (suffix === 'png') {
    domToImage.toPng(board).then(blob => {
      saveAs(blob, filename)
      showElements()
      callback()
    })
  } else if (suffix === 'jpeg') {
    const bg: HTMLElement = document.getElementById('background')
    bg.style.display = 'initial'
    domToImage.toJpeg(board).then(blob => {
      saveAs(blob, filename)
      bg.style.display = 'none'
      showElements()
      callback()
    })
  }
}

export function savePrompt(prompt: string, callback: () => any): void {
  if (window.confirm(prompt)) {
    const filename: string | null = window.prompt('Save as... (PNG and JPEG file types accepted)')
    if (filename !== null) {
      const parts: string[] = filename.split('.')
      const suffix: string = parts[parts.length-1]
      if (suffix === 'png' || suffix === 'jpeg') {
        exportWhiteboard(filename, suffix, callback)
      } else {
        console.log('Wrong file type, choosing default export')
        exportWhiteboard('whiteboard.jpeg', 'jpeg', callback)
      }
      return
    }
  }
  callback()
}

function exportAsPNG(board: HTMLElement): void {
  domToImage.toPng(board).then(blob => {
    saveAs(blob, 'whiteboard.png')
    showElements()
  })
}

function exportAsJPEG(board: HTMLElement): void {
  const bg: HTMLElement = document.getElementById('background')
  bg.style.display = 'initial'
  domToImage.toJpeg(board).then(blob => {
    saveAs(blob, 'whiteboard.jpeg')
    bg.style.display = 'none'
    showElements()
  })
}